<?php

namespace App\Repository;

use App\Entity\Kebab;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Kebab|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kebab|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kebab[]    findAll()
 * @method Kebab[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KebabRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Kebab::class);
    }
}
