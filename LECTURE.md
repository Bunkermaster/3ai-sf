Symfony 
===

# Youtube playlist

[Playlist must watch](https://youtu.be/qkblc5WRn-U)



# PhpStorm

## Plugins

* .ignore
* env file support
* PHP Annotations
* PHP composer support
* PHP Toolbox
* Symfony support

## Steps

### alias sf
```
➜  ~ alias sf
sf='php bin/console'
```

### homebrew mysql server start

```
brew services start mysql
```

or

```
mysql.server start
```

### conposer install

Installs the dependancies.

### Edit the .env file

Specify database connectivity settings.

### doctrine:database:create (d:d:c)

Creates the database.

### make:entity (m:e)

Creates an entity with you. Everytime you create a new attribute, the file is updated.

`/src/Entity`

Note: The database is **not** modified so far.

### make:migration (m:m)

Creates the migration file containing the necessary steps to update the database schema to mirror the entity modifications.

### doctrine:migrations:migrate (d:m:m)

Applies the migrations.

### make:crud (m:c)

Will generated the :

* controller
* templates
* form

### debug:router (d:r